import React, {useContext, useState} from 'react';
import gamesData from "../gamesData";
import GamesList from "../Components/GamesList";
import {StoreContext} from "../Store/store";

const filterResultsOnInput = (gamesList, inputFilterString) => {
  return gamesList.filter(function (item) {
    return item.name.toLowerCase().includes(inputFilterString.toLowerCase());
  });
};

const filterOnLabels = (gamesList, activeFeatureFilter) => {
  return gamesList.filter(function (item) {
    return item.labels.includes(`${activeFeatureFilter}`);
  });
};

function DefaultView({setActiveView}) {
  const { suggestions, isDoneLoading } = useContext(StoreContext)
  const [inputFilter, setInputFilter] = useState("");
  const [activeFeatureFilter, setActiveFeatureFilter] = useState(gamesData.allLabels[0]);

  function updateInputFilter(e){
    setInputFilter(e.target.value);
  }

  function toggleFeature(filterString){
    setActiveFeatureFilter(filterString);
  }

  let filteredSuggestions = suggestions ? suggestions.slice(0) : [];
  filteredSuggestions = filterResultsOnInput(filteredSuggestions, inputFilter);
  const enabledFilteredSuggestions = filterOnLabels(filteredSuggestions, activeFeatureFilter);

  return (
    <>
      <header id="home">
        <section className="hero">
          <div className="container">
            <div className="row text-center animated fadeInDown">
              <div className="col-md-12 wp4">
                <input className={"filter-input"} placeholder={"Search for a suggestion"} onChange={updateInputFilter}/>
              </div>
            </div>
            <div className="row text-center animated fadeInUp hero-button-row">
              <div className="col-md-12 wp4">
                {gamesData.allLabels.map((feature) => (
                  <button
                    key={feature}
                    className={`btn feature-btn ${activeFeatureFilter === feature ? "" : "inactive-filter-btn"}`}
                    onClick={() => toggleFeature(feature)}>{feature}</button>
                ))}
              </div>
              <div className="btn suggestions-button" onClick={()=>setActiveView("suggest-view")}>
                <i className={`far fa-plus-square`} /> Add suggestion
              </div>
            </div>
          </div>
        </section>
      </header>
      <section className="suggestions text-center section-padding">
        <h1>Suggestions</h1>
        {isDoneLoading
          ? (<GamesList suggestions={enabledFilteredSuggestions} />)
          : <p>loading data</p>
        }
      </section>
    </>
  );
}

export default DefaultView;
