import React from 'react';
import SuggestForm from "../Components/SuggestForm";

function DefaultView({setActiveView}) {
  return (
    <>
      <header id="home">
        <section className="hero">
          <div className="container">
            <div className="row text-center">
              <div className="btn suggestions-button" onClick={()=>setActiveView("default")}>
                <i className={`fa fa-angle-left`} /> Return to list
              </div>
            </div>
          </div>
        </section>
      </header>
      <section className="games text-center section-padding animated fadeInUp">
        <SuggestForm />
      </section>
    </>
  );
}

export default DefaultView;
