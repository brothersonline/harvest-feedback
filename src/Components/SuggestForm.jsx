import React, { useState} from 'react';
import "./SuggestForm.css";
import API_URL from "./constants";

function SuggestForm() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [email, setEmail] = useState(""); // Honeypot
  const [label, setLabel] = useState("game");
  const [formAlert, setFormAlert] = useState(false);
  const [formSend, setFormSend] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if(formSend || email !== ""){
      return;
    }

    setFormSend(true);

    if(!name){
      setFormAlert("Suggestion title is required.");
      return;
    }

    fetch(API_URL + 'create', {
      method: "POST",
      body: new URLSearchParams(`name=${name}&description=${description}&labels=${label}`),
    }).then(data => setFormAlert("Suggestion has been added successfully."));
  }

  return (
    <form onSubmit={handleSubmit} className={"suggest-form"}>
      <h1>Suggest something!</h1>
      <label className={"formlabel-hp"}>Email*</label>
      <input
        type="text"
        value={email}
        className={"field-hp"}
        onChange={(e) => setEmail(e.target.value)}
      />
      <label className={"formlabel"}>Title*</label>
        <input
          type="text"
          value={name}
          className={"field"}
          required={true}
          onChange={(e) => setName(e.target.value)}
        />
      <label className={"formlabel"}>Description</label>
      <textarea
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        className={"field"}
      />
      <label className={"formlabel"}>Suggestion type</label>
      <select className={"field"} onChange={(e) => setLabel(e.target.value)}>
        <option value="game">Game</option>
        <option value="discord">Discord</option>
        <option value="feedback-site">feedback site</option>
      </select>
      <input type="submit" className={"submit-button"} disabled={formSend} />
      {formAlert && <p>{formAlert}</p>}
    </form>
  );
}

export default SuggestForm;
