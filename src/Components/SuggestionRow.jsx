import React, {useContext, useState} from 'react';
import "./GamesList.css";
import API_URL from "./constants";
import {StoreContext} from "../Store/store";

const SuggestionRow = ({suggestion, vote}) =>{
  const { updateSuggestion, updateVote, revokeVote } = useContext(StoreContext);
  const [isVoting, setIsVoting] = useState(false);
  const hasVoted = !!vote;

  const upvote = () => {
    if(isVoting){
      return;
    }

    if(hasVoted){
      unvote();
      return;
    }
    setIsVoting(true);

    fetch(API_URL + 'upvote', {
      method: "POST",
      body: new URLSearchParams(`id=${suggestion.id}`),
    }).then(response=>response.json())
      .then(data=>{
        updateSuggestion(data);
        updateVote({suggestion_id: data.id, type: 'up'});
        setIsVoting(false);
    });
  };

  const downvote = () => {
    if(isVoting){
      return;
    }

    if(hasVoted){
      unvote();
      return;
    }
    setIsVoting(true);

    fetch(API_URL + 'downvote', {
      method: "POST",
      body: new URLSearchParams(`id=${suggestion.id}`),
    }).then(response=>response.json())
      .then(data => {
      updateSuggestion(data);
      updateVote({suggestion_id: data.id, type: 'down'});
      setIsVoting(false);
    });
  };

  const unvote = () => {
    setIsVoting(true);

    fetch(API_URL + 'unvote', {
      method: "POST",
      body: new URLSearchParams(`id=${suggestion.id}`),
    }).then(response=>response.json())
      .then(data => {
        updateSuggestion(data);
        revokeVote({suggestion_id: data.id});
        setIsVoting(false);
      });
  };

  const hasUpvoted = () =>{
    if (!hasVoted){
      return false;
    }
    if (!vote){
      return false;
    }
    if (!vote.type){
      return false;
    }
    return vote.type === 'up';
  }

  const hasDownvoted = () =>{
    if (!hasVoted){
      return false;
    }
    if (!vote){
      return false;
    }
    if (!vote.type){
      return false;
    }
    return vote.type === 'down';
  }

  return (
    <div className={"suggestion-row"} key={suggestion.name}>
      <div>
        {isVoting &&
          <>
            <button className={`votes-box 'disabled-vote-box' : ''}`} disabled={true}>
              <i className={`fa fa-spinner fa-spin`} />
            </button>
            <button className={`votes-box 'disabled-vote-box' : ''}`} disabled={true}>
            <i className={`fa fa-spinner fa-spin`} />
            </button>
          </>
        }
        {!isVoting &&
          <>
            <button className={`votes-box ${hasDownvoted() ? 'disabled-vote-box' : '' }`} onClick={()=> upvote(suggestion)} disabled={(hasDownvoted())}>
                <><i className={`fa fa-caret-up`}/> {suggestion.upvotes}</>
            </button>
            <button className={`votes-box ${hasUpvoted() ? 'disabled-vote-box' : '' }`} onClick={()=> downvote(suggestion)} disabled={hasUpvoted()}>
                <><i className={`fa fa-caret-down`}/> {suggestion.downvotes}</>
            </button>
          </>
        }
      </div>
      <div className={`suggestion-text-container`}>
        <h3 className={"suggestion-title"}>{suggestion.name}</h3>
        <p className={"content"}>{suggestion.description ? suggestion.description : ''}</p>
      </div>
    </div>
  );
};

export default SuggestionRow;
