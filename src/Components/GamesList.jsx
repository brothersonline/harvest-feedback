import React, {useContext} from 'react';
import "./GamesList.css";
import SuggestionRow from "./SuggestionRow";
import ModeratorRow from "./ModeratorRow";
import {StoreContext} from "../Store/store";

const getVoteForThisSuggestion = (suggestion,votes) => {
  if(votes[suggestion.id] !== undefined){
    return votes[suggestion.id]
  }
  return null;
};

function GamesList({suggestions = []}) {
  const { isLoggedIn, votes } = useContext(StoreContext)

  return (
    <div className="container">
      {suggestions.length === 0 &&
        <div>No suggestions were found</div>
      }
      <div className="row text-center animated fadeInUp">
        <div className="col-md-12 wp4 games-list">
          {suggestions.map((suggestion, i) => (
            <React.Fragment key={`game_${suggestion.id}`}>
              <SuggestionRow  suggestion={suggestion} vote={getVoteForThisSuggestion(suggestion, votes)}/>
              {isLoggedIn && <ModeratorRow suggestion={suggestion}/>}
            </React.Fragment>
            ))}
        </div>
      </div>
    </div>
  );
}

export default GamesList;
