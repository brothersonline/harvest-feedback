import React, {useState} from 'react';
import "./ModeratorRow.css";
import API_URL from "./constants";

function ModeratorRow({suggestion}) {
  const [isEnabled, setIsEnabled] = useState(suggestion.enabled === "1");
  const [isDoingCall, setIsDoingCall] = useState(false);
  const enableSuggestion = (suggestion) => {
    setIsDoingCall(true);
    fetch(API_URL + 'enable', {
      method: "POST",
      body: new URLSearchParams(`id=${suggestion.id}`),
    }).then(response=>response.json())
      .then(data => {
        setIsDoingCall(false);
        setIsEnabled(true)
      });
  };

  const disableSuggestion = (suggestion) => {
    setIsDoingCall(true);
    fetch(API_URL + 'disable', {
      method: "POST",
      body: new URLSearchParams(`id=${suggestion.id}`),
    }).then(response=>response.json())
      .then(data => {
        setIsDoingCall(false);
        setIsEnabled(false)
      });
  };

  return (
    <div className={"moderator-buttons"}>
      {!isEnabled
        ? (
          <button className={"mod-button"} onClick={()=> enableSuggestion(suggestion)}>
            {isDoingCall
              ? (<i className={`fa fa-spinner fa-spin`} />)
              : (<><i className={`fas fa-minus`}/> Disabled</>)
            }
          </button>
        ) : (
            <button className={"mod-button"} onClick={()=> disableSuggestion(suggestion)}>
              {isDoingCall
                ? (<i className={`fa fa-spinner fa-spin`} />)
                : (<><i className={`fas fa-check`}/> Enabled</>)
              }
            </button>
        )
      }
    </div>
  );
}

export default ModeratorRow;
