import React, {useEffect, useState} from 'react';
import API_URL from "../Components/constants";

export const StoreContext = React.createContext(null);

const getVotesMapping = (yourVotesData) => {
  const votesObj = {};

  yourVotesData.forEach((el)=>{
    votesObj[el.suggestion_id] = {id: el.suggestion_id, type: el.type};
  })

  return votesObj;
}

const getSuggestionKeyById = (suggestions, suggestion) => {
  for (let i = 0; i < suggestions.length; i++) {
    if(suggestions[i].id === suggestion.id){
      return i;
    }
  }
  return null;
}

const getVoteKeyById = (votes, vote) => {
  for (let i = 0; i < votes.length; i++) {
    if(votes[i].suggestion_id === vote.suggestion_id){
      return i;
    }
  }
  return null;
}

export default ({ children }) => {
  const [suggestionsData, setSuggestionsData] = useState({isLoading: true});
  const [yourVotesData, setYourVotesData] = useState({isLoading: true});
  const [isLoggedIn, setLoggedIn] = useState(false);
  const votesMapping = getVotesMapping(yourVotesData.votes ? yourVotesData.votes : []);

  const isDoneLoading = () => {
    return !yourVotesData.isLoading && !suggestionsData.isLoading;
  }

  const updateSuggestion = (suggestion) => {
    const i = getSuggestionKeyById(suggestionsData.suggestions, suggestion);
    const updatedSuggestions = {...suggestionsData};
    updatedSuggestions.suggestions[i] = suggestion;

    setSuggestionsData(updatedSuggestions);
  }

  const updateVote = (vote) => {
    const i = getVoteKeyById(yourVotesData.votes, vote);
    const updatedVotes = {...yourVotesData};

    if(i != null){
      console.log("Vote already exists? this cannot happen");
    } else {
      updatedVotes.votes.push(vote);
    }

    setYourVotesData(updatedVotes);
  }

  const revokeVote = (vote) => {
    const i = getVoteKeyById(yourVotesData.votes, vote);

    const updatedVotes = {...yourVotesData};
    updatedVotes.votes.splice(i, 1);
    setYourVotesData(updatedVotes);
  }

  useEffect(() => {
    fetch(API_URL + 'all')
      .then(response => response.json())
      .then(data =>  setSuggestionsData({ suggestions: data, isLoading: false }));

    fetch(API_URL + 'votes')
      .then(response => response.json())
      .then(data =>  setYourVotesData({ votes: data, isLoading: false }));

    fetch(API_URL + 'checkmod')
      .then(response => response.json())
      .then(data =>  setLoggedIn(data));
  }, [])

  const store = {
    suggestions: suggestionsData.suggestions || [],
    updateSuggestion: updateSuggestion,
    updateVote: updateVote,
    revokeVote: revokeVote,
    votes: votesMapping || [],
    isLoggedIn,
    isDoneLoading: isDoneLoading(),
  };

  // eslint-disable-next-line react/jsx-filename-extension
  return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
};
