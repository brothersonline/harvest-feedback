import React, { useState } from 'react';
import DefaultView from "./Views/DefaultView";
import SuggestView from "./Views/SuggestView";
import ReactGA from "react-ga4";

const trackingId = "G-K6X4PNKR9D";
ReactGA.initialize(trackingId);

function App() {
  ReactGA.pageview(window.location.pathname + window.location.search);
  const [activeView, setActiveView] = useState("default");

  return (
    <div className="App">
      {activeView === "default" && (<DefaultView setActiveView={setActiveView} />)}
      {activeView === "suggest-view" && (<SuggestView setActiveView={setActiveView} />)}
    </div>
  );
}

export default App;
